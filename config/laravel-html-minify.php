<?php
/**
 * LaravelHtmlMinify Config
 */

return [
    'minify'         => env('LARAVEL_HTML_MINIFY', true),
    'skip' => ['txt', 'pdf', 'doc', 'xls', 'ppt', 'zip', 'rar', 'iso', 'exe', 'docx']
];