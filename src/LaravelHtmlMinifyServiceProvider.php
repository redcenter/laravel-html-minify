<?php

namespace LaravelHtmlMinify;

use Illuminate\Support\ServiceProvider;

/**
 * Class LaravelHtmlMinifyServiceProvider
 *
 * @package LaravelHtmlMinify
 */
class LaravelHtmlMinifyServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishPackageAssets();
    }

    /**
     * publishPackageAssets
     */
    private function publishPackageAssets()
    {
        $this->publishes([
            $this->getPackageConfigPath() => config_path('laravel-html-minify.php'),
        ]);
    }

    /**
     * getPackageConfigPath
     *
     * @return string
     */
    private function getPackageConfigPath()
    {
        return __DIR__ . '/../config/laravel-html-minify.php';
    }
}
