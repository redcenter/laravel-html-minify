<?php

namespace LaravelHtmlMinify;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Config\Repository as Config;

/**
 * Class LaravelHtmlMinifyMiddleware
 *
 * @package LaravelHtmlMinify
 */
class LaravelHtmlMinifyMiddleware
{
    const CONFIG_SLUG_MINIFY = 'laravel-html-minify.minify';
    const CONFIG_SLUG_SKIP = 'laravel-html-minify.skip';
    const DEFAULT_SKIP_EXTENSIONS = ['txt', 'pdf', 'doc', 'xls', 'ppt', 'zip', 'rar', 'iso', 'exe', 'docx'];

    /**
     * @var string
     */
    private $content;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Config Container for injection
     */
    private $config;

    /**
     * MinifyHtmlMiddleware constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * handle
     *
     * @param         $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;
        $this->runDownstreamCodeToGetResponse($next);

        if ($this->isMinifyEnabled() && $this->extensionMustBeMinified()) {
            $this->getContentFromResponse();
            $this->deleteSpacesAfterNewLine();
            $this->deleteWhiteLinesFromContent();
            $this->deleteTabsFromContent();
            $this->deleteMultipleSpacesFromContent();
            $this->setContentBackOnResponse();
        }

        return $this->response;
    }

    /**
     * runDownstreamCodeToGetResponse
     *
     * @param Closure $next
     */
    private function runDownstreamCodeToGetResponse(Closure $next)
    {
        $this->response = $next($this->request);
    }

    /**
     * getContentFromResponse
     */
    private function getContentFromResponse()
    {
        $this->content = $this->response->getContent();
    }

    /**
     * deleteSpacesAfterNewLine
     */
    private function deleteSpacesAfterNewLine()
    {
        $pattern = '/[\n] +/';
        $this->replaceInContent($pattern, "\n");
    }

    /**
     * deleteWhiteLinesFromContent
     */
    private function deleteWhiteLinesFromContent()
    {
        $pattern = '/[\n\r]+/';
        $this->replaceInContent($pattern, '');
    }

    /**
     * deleteTabsFromContent
     */
    private function deleteTabsFromContent()
    {
        $pattern = '/\t+/';
        $this->replaceInContent($pattern, '');
    }

    /**
     * deleteMultipleSpacesFromContent
     */
    private function deleteMultipleSpacesFromContent()
    {
        $pattern = '/  +/';
        $replacement = ' ';
        $this->replaceInContent($pattern, $replacement);
    }

    /**
     * setContentBackOnResponse
     */
    private function setContentBackOnResponse()
    {
        $this->response->setContent($this->content);
    }

    /**
     * replaceInContent
     *
     * @param $pattern
     * @param $replacement
     */
    private function replaceInContent($pattern, $replacement)
    {
        $this->content = preg_replace($pattern, $replacement, $this->content);
    }

    /**
     * isMinifyEnabled
     *
     * @return boolean
     */
    private function isMinifyEnabled()
    {
        return $this->config->get(self::CONFIG_SLUG_MINIFY, false);
    }

    /**
     * extensionMustBeMinified
     *
     * @return bool
     */
    private function extensionMustBeMinified()
    {
        $allExtensionsToSkip = collect($this->config->get(self::CONFIG_SLUG_SKIP, self::DEFAULT_SKIP_EXTENSIONS));
        $routePatternsToSkip = $allExtensionsToSkip->map(function ($item) {
            return sprintf('*.%s', $item);
        })->all();
        return !$this->request->is($routePatternsToSkip);
    }
}