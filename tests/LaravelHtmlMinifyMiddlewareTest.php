<?php

namespace LaravelHtmlMinify\Tests;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use LaravelHtmlMinify\LaravelHtmlMinifyMiddleware;
use Mockery;
use Illuminate\Config\Repository as Config;
use Orchestra\Testbench\TestCase;

/**
 * Class LaravelHtmlMinifyMiddlewareTest
 *
 * @package LaravelHtmlMinify\Tests
 */
class LaravelHtmlMinifyMiddlewareTest extends TestCase
{
    const CONFIG_SLUG_MINIFY = 'laravel-html-minify.minify';
    const CONFIG_SLUG_SKIP = 'laravel-html-minify.skip';
    const DEFAULT_SKIP_EXTENSIONS = ['txt', 'pdf', 'doc', 'xls', 'ppt', 'zip', 'rar', 'iso', 'exe', 'docx'];

    /**
     * @var LaravelHtmlMinifyMiddleware
     */
    private $class;

    /**
     * @var array
     */
    private $mocks = [];

    /**
     * testHtmlDoesNotContainNewLines
     *
     * @dataProvider dataProviderContent
     *
     * @param $arrangedContent
     * @param $expectedContent
     */
    public function testHtmlDoesNotContainSpecificContent($arrangedContent, $expectedContent)
    {

        /**
         * Arrange
         */
        $this->mocks['Config']->shouldReceive('get')->with(self::CONFIG_SLUG_MINIFY, false)->once()->andReturn(true);
        $this->mocks['Config']->shouldReceive('get')->with(self::CONFIG_SLUG_SKIP,
            self::DEFAULT_SKIP_EXTENSIONS)->once()->andReturn([]);
        $this->mocks['Request']->shouldReceive('is')->with([])->once()->andReturn(false);
        $this->prepareClassToBeTested();
        $response = $this->createResponseWithContent($arrangedContent);
        $closure = $this->createClosureWhichReturnsResponse($response);

        /**
         * Act
         */
        $response = $this->class->handle($this->mocks['Request'], $closure);

        /**
         * Assert
         */
        $this->assertEquals($expectedContent, $response->getContent());

    }

    /**
     * dataProviderContent
     *
     * @return array
     */
    public function dataProviderContent()
    {
        $output = [];

        $scenario = 'spaces after new lines';
        $arrangedContent = "\n Look! I am content \n \nwithout any spaces after \nwhite lines or \n\rreturns\n";
        $expectedContent = 'Look! I am content without any spaces after white lines or returns';
        $output[$scenario] = [$arrangedContent, $expectedContent];

        $scenario = 'new lines';
        $arrangedContent = "\nLook! I am content \n\nwithout any \nwhite lines or \n\rreturns\n";
        $expectedContent = 'Look! I am content without any white lines or returns';
        $output[$scenario] = [$arrangedContent, $expectedContent];

        $scenario = 'tabs';
        $arrangedContent = "Look! I am content \twithout any \t\ttabs";
        $expectedContent = 'Look! I am content without any tabs';
        $output[$scenario] = [$arrangedContent, $expectedContent];

        $scenario = 'multiple spaces';
        $arrangedContent = "Look!   I am    content without multiple  spaces";
        $expectedContent = 'Look! I am content without multiple spaces';
        $output[$scenario] = [$arrangedContent, $expectedContent];

        return $output;
    }


    public function testWhenDisabledContentStaysTheSame()
    {
        /**
         * Arrange
         */
        $this->mocks['Config']->shouldReceive('get')->with(self::CONFIG_SLUG_MINIFY, false)->once()->andReturn(false);
        $this->mocks['Config']->shouldReceive('get')->never();
        $this->prepareClassToBeTested();
        $arrangedContent = "Look!   I am    content without multiple  spaces";
        $arrangedContent .= "Look! I am content \twithout any \t\ttabs";
        $arrangedContent .= "\n Look! I am content \n \nwithout any spaces after \nwhite lines or \n\rreturns\n";
        $arrangedContent .= "\nLook! I am content \n\nwithout any \nwhite lines or \n\rreturns\n";
        $expectedContent = $arrangedContent;
        $response = $this->createResponseWithContent($arrangedContent);
        $closure = $this->createClosureWhichReturnsResponse($response);

        /**
         * Act
         */
        $response = $this->class->handle($this->mocks['Request'], $closure);

        /**
         * Assert
         */
        $this->assertEquals($expectedContent, $response->getContent());
    }

    public function testWhenExtensionMustBeSkipped()
    {
        /**
         * Arrange
         */
        $extensionsToSkip = ['bla', 'etc'];
        $isCheck = ['*.bla', '*.etc'];
        $this->mocks['Config']->shouldReceive('get')->with(self::CONFIG_SLUG_MINIFY, false)->once()->andReturn(true);
        $this->mocks['Config']->shouldReceive('get')->with(self::CONFIG_SLUG_SKIP,
            self::DEFAULT_SKIP_EXTENSIONS)->once()->andReturn($extensionsToSkip);
        $this->mocks['Request']->shouldReceive('is')->with($isCheck)->once()->andReturn(true);
        $this->prepareClassToBeTested();
        $arrangedContent = "Look!   I am    content without multiple  spaces";
        $arrangedContent .= "Look! I am content \twithout any \t\ttabs";
        $arrangedContent .= "\n Look! I am content \n \nwithout any spaces after \nwhite lines or \n\rreturns\n";
        $arrangedContent .= "\nLook! I am content \n\nwithout any \nwhite lines or \n\rreturns\n";
        $expectedContent = $arrangedContent;
        $response = $this->createResponseWithContent($arrangedContent);
        $closure = $this->createClosureWhichReturnsResponse($response);

        /**
         * Act
         */
        $response = $this->class->handle($this->mocks['Request'], $closure);

        /**
         * Assert
         */
        $this->assertEquals($expectedContent, $response->getContent());
    }


    /**
     * createClosureWhichReturnsResponse
     *
     * @param $response
     *
     * @return Closure
     */
    private function createClosureWhichReturnsResponse($response)
    {
        return function () use ($response) {
            return $response;
        };
    }

    /**
     * createResponseWithContent
     *
     * @param $content
     *
     * @return Response
     */
    private function createResponseWithContent($content)
    {
        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    /**
     * prepareClassToBeTested
     */
    private function prepareClassToBeTested()
    {
        $this->class = new LaravelHtmlMinifyMiddleware($this->mocks['Config']);
    }

    /**
     * setUp
     */
    public function setUp()
    {
        $this->mocks['Config'] = Mockery::mock(Config::class);
        $this->mocks['Request'] = Mockery::mock(Request::class);
    }
}
